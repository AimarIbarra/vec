# VEC
A simple example of a type generic dynamic array in C.
Be warned, the static analyzer will yell at you for compiling this!

## Running the example
```sh
make run
```

## Documentation
I will add doc comments in the future if this thing ends up being something.

## Author
Aimar Ibarra Viadero <aimaribarraviadero@gmail.com>

## License
Licensed under GPL3, see LICENSE
