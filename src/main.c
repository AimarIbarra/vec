#include "str.h"
#include <ctype.h>

#define STR1(x) #x
#define STR(x) STR1(x)

int main(void) {
  printf("Enter characters to caesar chiper: ");
  char *line = vec_alloc(256);
  sgetline(stdin, &line, 256);

  printf("Enter the amount of rotation: ");
  int rot;
  int status;
  while ((status = scanf("%d", &rot)) != 1 || rot < 0) {
    if (status == EOF) {
      printf("EOF\n");
      return 1;
    }
    printf("Invalid input, try again: ");
    scanf("%*s");
  }
 
  vec_each(line, c) {
    if (islower(*c)) {
      *c = (*c - 'a' + rot) % ('z' - 'a' + 1) + 'a';
    } else if (isupper(*c)) {
      *c = (*c - 'A' + rot) % ('Z' - 'A' + 1) + 'A';
    }
  }
  vadd(&line, '\0');

  printf("Result: %s\n", line);
  vec_free(line);

  return 0;
}
