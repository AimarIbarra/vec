#ifndef STR_H_INCLUDED
#define STR_H_INCLUDED

#include <stdio.h>
#include "vec.h"

[[nodiscard("str_dup allocates memory, you should call vec_free on it")]]
char *str_dup(const char *str);
void sgetdelim(FILE *input, char **s, size_t size, char delim);
#define sgetline(f, v, s) sgetdelim(f, v, s, '\n')

#endif
