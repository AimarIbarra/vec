#ifndef VEC_H_INCLUDED
#define VEC_H_INCLUDED

#include <stddef.h>

[[nodiscard("vec_alloc allocates memory, you should call vec_free on it.")]]
void *vec_alloc(size_t capacity);

[[nodiscard("vec_make allocates memory, you should call vec_free on it.")]]
void *vec_make(const void *data, size_t size);
#define vmake(arr) vec_make(arr, sizeof(arr))

void vec_free(void *vec);

void *vec_add(void **vec, size_t bytes);
#define vadd(vecptr, val) do {                                        \
  auto _vec_save = vecptr;                                            \
  void *_vec_ptr = *_vec_save;                                        \
  *(typeof(*_vec_save))vec_add(&_vec_ptr, sizeof(**_vec_save)) = val; \
  *_vec_save = _vec_ptr; } while (0)

#define _vec_ptr_const
#define vec_each(vec, it) \
  for (typeof_unqual(vec) it = vec, _vec_step = it, _vec_end = (void *)((char *)it + vec_size(it)); _vec_step < _vec_end; ++_vec_step, ++it)

size_t vec_size(void *vec);
#define vsize(vec) (vsize(vec)/sizeof(*(vec)))

void vec_reserve(void **vec, size_t size);
#define vreserve(vec, size) vec_reserve_(vec, sizeof(*(vec))*size)

void vec_empty(void *vec);

#endif
