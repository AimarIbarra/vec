#include "vec.h"
#include "str.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#ifdef __GNUC__
#define FALLTHROUGH __attribute__((fallthrough));
#else
#define FALLTHROUGH [[fallthrough]]
#endif

[[noreturn]]
static void die(void) {
  perror("vec library");
  abort();
}

typedef struct {
  size_t size;
  size_t capacity;
  [[maybe_unused]]
  max_align_t data[];
} Vec;

static void reserve(Vec **vec, size_t size) {
  Vec *v = *vec;
  if (v->capacity >= size)
    return;
  if (v->capacity == 0) {
    v->capacity = size;
  } else {
    do {
      v->capacity <<= 1;
    } while (v->capacity < size);
  }
  *vec = realloc(v, sizeof(Vec) + v->capacity);
  if (!*vec)
    die();
}

static void *add(Vec **vec, size_t size) {
  reserve(vec, vec[0]->size + size);
  void *sbrk = (char *)vec[0]->data + vec[0]->size;
  vec[0]->size += size;
  return sbrk;
}

static Vec *extract(void *vec) {
  return (Vec *)((char *)vec - offsetof(Vec, data));
}

void vec_empty(void *v) {
  extract(v)->size = 0;
}

[[nodiscard("vec_alloc allocates memory, you should call vec_free on it.")]]
void *vec_alloc(size_t capacity) {
  Vec *v = malloc(sizeof(Vec) + capacity);
  if (!v)
    die();
  v->size = 0;
  v->capacity = capacity;
  return v->data;
}

[[nodiscard("vec_make allocates memory, you shoud call vec_free on it.")]]
void *vec_make(const void *data, size_t size) {
  Vec *v = malloc(sizeof(Vec) + size);
  if (!v)
    die();
  v->size = size;
  v->capacity = size;
  return memcpy(v->data, data, size);
}

void vec_free(void *vec) {
  free(extract(vec));
}

void *vec_add(void **vec, size_t size) {
  Vec *v = extract(*vec);
  void *r = add(&v, size);
  *vec = v->data;
  return r;
}

void vec_reserve(void **vec, size_t size) {
  Vec *v = extract(vec);
  reserve(&v, size);
  *vec = v->data;
}

size_t vec_size(void *vec) {
  return extract(vec)->size;
}

[[nodiscard("str_dup allocates memory, you should call vec_free on it.")]]
char *str_dup(const char *str) {
  return vec_make(str, strlen(str));
}

void sgetdelim(FILE *input, char **v, size_t siz, char delim) {
  vec_empty(*v);
  char *readptr = *v;
  char fmt[3 * sizeof(size_t) * CHAR_BIT / 8 + sizeof("[^d]%zn%c")];
  sprintf(fmt, "%%%zu[^%c]%%zn%%c", siz, delim);

  while (1) {
    size_t read;
    char ch;
    void *ptr;

    enum {
      EMPTY_LINE = 0,
      LARGER_SIZ = 2,
      FIT_SIZ = 1,
    };
      
    switch (fscanf(input, fmt, readptr, &read, &ch)) {
    case EMPTY_LINE:
      // get rid of newline
      getc(input);

    FALLTHROUGH
    case EOF:
      return;

    case LARGER_SIZ:
      // If the byte that overflowed isn't a newline
      if (ch != delim) {
        ptr = *v;
        readptr = (char *)vec_add(&ptr, read + 1) + read;
        *v = ptr;
        *readptr++ = ch;
        break;
      }

    FALLTHROUGH
    case FIT_SIZ:
      ptr = *v;
      vec_add(&ptr, read);
      *v = ptr;
      return;
    }
  }
}
