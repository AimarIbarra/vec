TARGET := prog
DEBUGGER := gdb

CFLAGS := -Wall -Wextra -Werror -pedantic -pedantic-errors -std=c2x

SRCDIR := src
BINDIR := .bin

SRCS = main.c vec.c

OBJS = $(SRCS:%.c=$(BINDIR)/%.o)

.PHONY: run clean debug optimize

$(BINDIR)/$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) $^ $(LOADLIBES) -o $@

$(BINDIR)/%.o: $(SRCDIR)/%.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $< -o $@

clean:
	rm -fr $(BINDIR)

optimize: CFLAGS += -O3 -flto
optimize: $(BINDIR)/$(TARGET)

run: CFLAGS += -O0
run: $(BINDIR)/$(TARGET)
	@exec $< $(ARGV)

debug: CFLAGS += -g -O2
debug: $(BINDIR)/$(TARGET) debugbuild
	@exec $(DEBUGGER) --args $< $(ARGV)
